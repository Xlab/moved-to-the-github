# ПЕРЕЕЗД НА GITHUB ЗАВЕРШЁН

### На идею переехать окончательно столкнула моя любимая книжка по СКВ
[Eric Sink: Version control by example](http://www.amazon.com/Version-Control-Example-Eric-Sink/dp/0983507902)

### Накатал статью о том, как удобно переезжать (и почему переехал я)
[Конвертация Mercurial в Git](http://kc.vc/blog/2012/06/mercurial-to-git.html)

### Для тех, кто хочет поглубже освоить Git, очень советую
[Jon Loeliger: Version Control with Git](http://www.amazon.com/Version-Control-Git-Jon-Loeliger/dp/8184047320).

![pyotr](http://cl.ly/Hp2X/pyotr.png)